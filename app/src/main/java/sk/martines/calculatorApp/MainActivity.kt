package sk.martines.calculatorApp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import sk.martines.calculatorApp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var numbers: ArrayList<Float>? = null
    private var chars: ArrayList<String>? = null

    private var number = ""
    private var example = ""
    private var result = 0f
    private var isReset = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        numbers = ArrayList()
        chars = ArrayList()

        binding.oneBtn.setOnClickListener {
            enterNumber("1")
        }
        binding.twoBtn.setOnClickListener {
            enterNumber("2")
        }
        binding.threeBtn.setOnClickListener {
            enterNumber("3")
        }
        binding.fourBtn.setOnClickListener {
            enterNumber("4")
        }
        binding.fiveBtn.setOnClickListener {
            enterNumber("5")
        }
        binding.sixBtn.setOnClickListener {
            enterNumber("6")
        }
        binding.sevenBtn.setOnClickListener {
            enterNumber("7")
        }
        binding.eightBtn.setOnClickListener {
            enterNumber("8")
        }
        binding.nineBtn.setOnClickListener {
            enterNumber("9")
        }
        binding.zeroBtn.setOnClickListener {
            enterNumber("0")
        }

        binding.additionBtn.setOnClickListener {
            enterChar("+")
        }
        binding.subtractionBtn.setOnClickListener {
            enterChar("-")
        }
        binding.multiplicationBtn.setOnClickListener {
            enterChar("*")
        }
        binding.divisionBtn.setOnClickListener {
            enterChar("/")
        }
        binding.equalsBtn.setOnClickListener {
            if (isReset && number != "") {
                numbers?.add(number.toFloat())
                calculateExample()
            }
        }
        binding.resetBtn.setOnClickListener {
            isReset = true
            number = ""
            example = ""
            result = 0f
            numbers = ArrayList()
            chars = ArrayList()
            binding.resultTxt.text = ""
            binding.exampleTxt.text = ""
        }
    }

    private fun enterNumber(n: String) {
        if (isReset) {
            number += n
            example += n
            binding.exampleTxt.text = example
        }
    }

    private fun enterChar(ch: String) {
        if (isReset && number != "") {
            numbers?.add(number.toFloat())
            number = ""
            chars?.add(ch)
            example += ch
            binding.exampleTxt.text = example
        }
    }

    private fun calculateExample() {
        if (numbers!!.size != 0 && chars!!.size != 0 && numbers!!.size > chars!!.size) {
            result = numbers!![0]
            var i = 0
            while (i < chars!!.size) {
                if (chars!![i] == "+") {
                    result += numbers!![i + 1]
                    i++
                } else if (chars!![i] == "-") {
                    result -= numbers!![i + 1]
                    i++
                } else if (chars!![i] == "*") {
                    result *= numbers!![i + 1]
                    i++
                } else if (chars!![i] == "/") {
                    if (numbers!![i + 1] != 0f) {
                        result /= numbers!![i + 1]
                        i++
                    } else {
                        binding.resultTxt.text = "ERROR"
                        return
                    }
                }
            }
            binding.resultTxt.text = result.toString()
            isReset = false
        }
    }
}